import Http from '../../utils/http';
const Apples = {
  init:() => { return Http.get('/apples',null); },
  pick:() => { return Http.get('/apples/pick',null);},
};
export default Apples;
