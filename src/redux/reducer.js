import { combineReducers } from 'redux';
import appleReducer from './reducers/appleReducer';
const rootReducer = combineReducers({
  appleBasket: appleReducer
});

export default rootReducer;
