const initialState = {
  isPicker:false,
  apples:[]
};

const appleReducer = (state=initialState,action) =>{
  switch (action.type) {

    case 'apple/INIT_APPLE_SUCCESS':
      state = action.payload;
      return {...state};

    case 'apple/INIT_APPLE_FAIL':
      return state;

    case 'apple/EAT_APPLE':
      state.apples.map(elem => {
        if (elem.id===action.payload) {
          elem.isEaten = true;
        }
      })
      return {...state};

    case 'apple/BEGIN_PICK_APPLE':
      state.isPicker = true;
      return {...state};

    case 'apple/DONE_PICK_APPLE':
      state.isPicker = false;
      state.apples.push(...action.payload);
      return {...state};

    case 'apple/FAIL_PICK_APPLE':
      state.isPicker = false;
      return {...state};

    default:
      return state;
  }
}
export default appleReducer;
