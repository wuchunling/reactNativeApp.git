import Http from '../utils/http'

export const User= {
  //获取登录用户信息
  info:function(){ return Http.get('/apples',null); },
  //发送短信验证码
  code:function(phone){ return Http.get(`sms/code?phone=${phone}`,null); },
  //登录
  login:function(phone,code,user){ return Http.post('user/login',{phone:phone,code:code,user:user}); },
}
