import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View } from 'react-native'
import AppleBasket from '../components/AppleBasket';
import actions from '../redux/actions/appleAction';

class Apples extends React.Component {

  constructor(props){  //初始化数据
    super(props);
    this.props.dispatch(this.props.actions.initApplesStart)
  }
  render(){
    let {appleBasket,actions,dispatch} = this.props;
    return(
      <View>
        <AppleBasket appleBasket={appleBasket} actions={actions}/>
      </View>
    )
  }
}

const mapStateToProps = state => ({
    appleBasket: state.appleBasket
});
const mapDispatchToProps = dispatch => ({
    dispatch: dispatch,
    actions: bindActionCreators(actions, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Apples);
