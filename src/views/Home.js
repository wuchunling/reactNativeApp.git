import React from 'react'
import { View,StyleSheet,Text,Image,Button } from 'react-native'

class Home extends React.Component{
  static navigationOptions = {
      tabBarLabel: '首页',
      tabBarIcon: ({focused}) => {
          if (focused) {
              return (
                  <Image style={styles.tabBarIcon} source={require('../assets/navbar-home-icon-active.png')}/>
              );
          }
          return (
              <Image style={styles.tabBarIcon} source={require('../assets/navbar-home-icon.png')}/>
          );
      },
  };
  render(){
    const {navigate} = this.props.navigation;
    return(
      <View style={styles.container}>
        <Text>首页</Text>
        <Button
          onPress={() => navigate('Apples', {name: 'Brent'})}
          title="苹果篮子"
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  tabBarIcon:{
    width:24,height:24
  },
  container:{
    padding:30
  }
});
export default Home;
