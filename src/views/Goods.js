import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { View,StyleSheet,Text,Image } from 'react-native'

class Detail extends React.Component{
  static navigationOptions = {
      tabBarLabel: '商品',
      tabBarIcon: ({focused}) => {
          if (focused) {
              return (
                  <Image style={styles.tabBarIcon} source={require('../assets/navbar-goods-icon-active.png')}/>
              );
          }
          return (
              <Image style={styles.tabBarIcon} source={require('../assets/navbar-goods-icon.png')}/>
          );
      },
  };
  render(){
    return(
      <Text>
        商品页面
      </Text>
    )
  }
}
const styles = StyleSheet.create({
  tabBarIcon:{
    width:24,height:24
  }
});
export default Detail;
