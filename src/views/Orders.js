import React from 'react'
import { View,StyleSheet,Text,Image } from 'react-native'

class Orders extends React.Component{
  static navigationOptions = {
      tabBarLabel: '订单',
      tabBarIcon: ({focused}) => {
          if (focused) {
              return (
                  <Image style={styles.tabBarIcon} source={require('../assets/navbar-order-icon-active.png')}/>
              );
          }
          return (
              <Image style={styles.tabBarIcon} source={require('../assets/navbar-order-icon.png')}/>
          );
      },
  };
  render(){
    return(
      <Text>订单页面</Text>
    )
  }
}
const styles = StyleSheet.create({
  tabBarIcon:{
    width:24,height:24
  }
});
export default Orders;
