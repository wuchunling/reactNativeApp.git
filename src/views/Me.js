import React from 'react'
import { View,StyleSheet,Text,Image } from 'react-native'

class Me extends React.Component{
  static navigationOptions = {
      tabBarLabel: '我的',
      tabBarIcon: ({focused}) => {
          if (focused) {
              return (
                  <Image style={styles.tabBarIcon} source={require('../assets/navbar-me-icon-active.png')}/>
              );
          }
          return (
              <Image style={styles.tabBarIcon} source={require('../assets/navbar-me-icon.png')}/>
          );
      },
  };
  render(){
    return(
      <Text>个人中心</Text>
    )
  }
}
const styles = StyleSheet.create({
  tabBarIcon:{
    width:24,height:24
  }
});
export default Me;
