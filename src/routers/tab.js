import React from 'react';
import {createBottomTabNavigator} from 'react-navigation';

import Home from '../views/Home'
import Me from '../views/Me'
import Goods from '../views/Goods'
import Orders from '../views/Orders'

const TabNav = createBottomTabNavigator(
  {
    Home:{screen:Home},
    Goods:{screen:Goods},
    Orders:{screen:Orders},
    Me:{screen:Me},
  },{
    initialRouteName: 'Home',
    tabBarOptions:{
      //当前选中的tab bar的文本颜色和图标颜色
      activeTintColor: '#FF7900',
      //当前未选中的tab bar的文本颜色和图标颜色
      inactiveTintColor: '#b2b2b2',
      //是否显示tab bar的图标，默认是false
      showIcon: true,
      //showLabel - 是否显示tab bar的文本，默认是true
      showLabel: true,
      //是否将文本转换为大小，默认是true
      upperCaseLabel: false,
      //material design中的波纹颜色(仅支持Android >= 5.0)
      pressColor: '#788493',
      //按下tab bar时的不透明度(仅支持iOS和Android < 5.0).
      pressOpacity: 0.8,
      //tab bar的样式
      style: {
          backgroundColor: '#fff',
          paddingBottom: 2,
          paddingTop:2,
          borderTopWidth: 0.1,
          borderTopColor: '#b2b2b2',
      },
      //tab bar的文本样式
      labelStyle: {
          fontSize:12,
          margin: 1
      },
      //tab 页指示符的样式 (tab页下面的一条线).
      indicatorStyle: {height: 0}
    },
    tabBarPosition:'bottom', //tab bar的位置, 可选值： 'top' or 'bottom'
    swipeEnabled: true, //是否允许滑动切换tab页
    animationEnabled: false,  //是否在切换tab页时使用动画
    lazy: true, //是否懒加载
    backBehavior: 'none' //返回按钮是否会导致tab切换到初始tab页？ 如果是，则设置为initialRoute，否则为none。 缺省为initialRoute。
  }
);
export default TabNav;
