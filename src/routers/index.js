import { StackNavigator,TabNavigator,addNavigationHelpers } from 'react-navigation';
import TabNav from './tab'
import Apples from '../views/Apples'
import Home from '../views/Home'
const  AppNavigator = StackNavigator({ //整个应用的路由栈
    TabBar: {
        screen: TabNav,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    Apples: {screen: Apples}
  }
);

export default AppNavigator;
