import React,{Component} from 'react'
import {Image,Text,View,StyleSheet} from 'react-native'
import Button from 'react-native-button';
import AppleItem from './AppleItem'

export default class AppleBasket extends Component{
  constructor(props) {
    super(props);
  }
  render(){
    const that = this;
    const {appleBasket,actions} = this.props;
    let stats = {
      isPicker:appleBasket.isPicker,
      eat:{num:0,weight:0},
      noeat:{num:0,weight:0},
      apples:[]
    }
    appleBasket.apples.map(elem => {
      let name = elem.isEaten?"eat":"noeat";
      stats[name].num++;
      stats[name].weight += elem.weight;
      if (!elem.isEaten) {
        stats.apples.push(elem);
      }
    })
    function getNoApple(){
      if (stats.apples.length===0 && !stats.isPicker) {
        return <Text style={styles.noApple}>篮子空空如也，快去摘苹果吧</Text>
      }
      return;
    }
    function getApples(){
      let data = [];
      if (!stats.isPicker) {
        data.push(stats.apples.map((apple,index) => <AppleItem key={index} apple={apple} eatApple={that.props.actions.eatApple}/> ))
      }else{
        return <Text style={styles.noApple}>正在采摘苹果...</Text>
      }
      return data;
    }
    return(
      <View style={styles.basket}>
        <Text style={styles.title}>苹果篮子</Text>
        <View style={[styles.row,styles.statsPanel]}>
          <View style={styles.status}>
            <Text style={styles.statusText}>当前</Text>
            <Text style={styles.num}>{stats.noeat.num}个苹果，{stats.noeat.weight}克</Text>
          </View>
          <View style={styles.status}>
            <Text style={styles.statusText}>已吃掉</Text>
            <Text style={styles.num}>{stats.eat.num}个苹果，{stats.eat.weight}克</Text>
          </View>
        </View>
        <View style={styles.col}>
          {getApples()}
          {getNoApple()}
        </View>
        <Button style={styles.pickBtn} onPress={actions.pickApple}>摘苹果</Button>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  row:{flexDirection:'row',alignItems:'center'},
  col:{alignItems:'center'},
  basket:{
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:'#eee',
    borderRadius:5,
    paddingVertical:5,
    paddingHorizontal:16,
    margin:10
  },
  title:{
    color:'#00669a',
    fontSize:18,
    lineHeight:50,
    textAlign:'center'
  },
  statsPanel:{
    borderColor:'#eee',
    borderWidth:1,
    borderRadius:5,
    marginBottom:12,
    paddingHorizontal:12,
    height:60
  },
  status:{
    paddingVertical:8,
    paddingHorizontal:8,
    flex:1
  },
  statusText:{
    color:'#00669e',
    fontSize:16
  },
  num:{
    fontSize:14,
    color:'gray',
    marginTop:4
  },
  pickBtn:{
    backgroundColor:'#00669e',
    color:'white',
    fontSize:16,
    borderRadius:5,
    width:100,
    height:35,
    lineHeight:35,
    marginTop:20,
    marginBottom:12,
    justifyContent:'center'
  },
  noApple:{
    fontSize:18,
    color:'gray'
  }
});
