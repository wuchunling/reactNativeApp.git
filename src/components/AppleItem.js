import React,{Component} from 'react'
import {Image,Text,View,StyleSheet} from 'react-native'
import Button from 'react-native-button';

export default class AppleItem extends Component{
  constructor(props) {
    super(props);
  }
  render(){
    let {apple,eatApple} = this.props;
    return(
      <View style={[styles.row,styles.apple]}>
        <View style={styles.row}>
          <Image source={require("../assets/apple.png")} style={styles.img}/>
          <View>
            <Text style={styles.title}>{apple.title}</Text>
            <Text style={styles.weight}>{apple.weight}克</Text>
          </View>
        </View>
        <Button style={styles.eatBtn} onPress={eatApple.bind(this,apple.id)}>吃掉</Button>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  apple:{
    width:360,
    justifyContent:'space-between',
    backgroundColor:'white',
    paddingVertical:5,
    paddingHorizontal:8,
    borderColor:'#eee',
    borderWidth:1,
    borderRadius:5,
    marginBottom:12
  },
  img:{
    marginRight:8
  },
  row:{
    flexDirection:'row',
    alignItems:'center'
  },
  title:{
    color:'#00669a',
    fontSize:16
  },
  weight:{
    color:'gray',
    fontSize:14
  },
  eatBtn:{
    borderRadius:5,
    backgroundColor:'#00669A',
    color:'#fff',
    width:70,
    fontSize:14,
    lineHeight:30,
    borderWidth:0
  }
})
