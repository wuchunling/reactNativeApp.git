import axios from 'axios';
axios.defaults.baseURL = "https://easy-mock.com/mock/5b4bf8299972135bfccea463/example";
// axios.defaults.baseURL = 'http://gitlab.ele233.com:9080/api';
const global = {
  get:function(url,data,sucCb,errCb){
    axios.get(url,{params:data})
    .then(function(res){
      if (res.data.code==200) {
        sucCb(res.data.data)
      }else {
        errCb(res.data.message);
      }
    })
    .catch(function(err){
      errCb(err)
    })
  },
  post:function(url,data,sucCb,errCb){
    axios.post(url,data)
    .then(function(res){
      if (res.data.code==200) {
        sucCb(res.data.data)
      }else {
        errCb(res.data.message);
      }
    })
    .catch(function(err){
      errCb(err)
    })
  },
  put:function(url,data,sucCb,errCb){
    axios.put(url,data)
    .then(function(res){
      if (res.data.code==200) {
        sucCb(res.data.data)
      }else {
        errCb(res.data.message);
      }
    })
    .catch(function(err){
      errCb(err)
    })
  },
  delete:function(url,sucCb,errCb){
    axios.delete(url)
    .then(function(res){
      if (res.data.code==200) {
        sucCb(res.data.data)
      }else {
        errCb(res.data.message);
      }
    })
    .catch(function(err){
      errCb(err)
    })
  }
}
const Http = {
  get:function(url,data){
    return new Promise((resolve, reject) => {
      global.get(url,data,function(res){
        // alert('发送请求成功');
        resolve(res);
      },function(err){
        reject(err);
      });
    }).catch(function(e) {
      // alert('发送请求失败')
      alert(e);
    });
  },
  post:function(url,data){
    return new Promise((resolve, reject) => {
      global.post(url,data,function(res){
        resolve(res);
      },function(err){
        reject(err);
      });
    }).catch(function(e) {
      alert(e);
    });
  },
  put:function(url,data){
    return new Promise((resolve, reject) => {
      global.put(url,data,function(res){
        alert('操作成功!');
        resolve(res);
      },function(err){
        reject(err);
      });
    }).catch(function(e) {
      alert(e);
    });
  },
  delete:function(url,data){
    return new Promise((resolve, reject) => {
      global.delete(url,data,function(res){
        alert('操作成功!');
        resolve(res);
      },function(err){
        reject(err);
      });
    }).catch(function(e) {
      alert(e);
    });
  }
}
export default Http;
