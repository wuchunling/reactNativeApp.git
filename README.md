# reactNativeApp

#### 项目介绍
搭建好的react native项目架构

#### 软件架构
安装依赖包
react / react-native / axios /redux / react-navigation
redux-thunk / react-redux
react-native-button / react-native-storage

#### 安装教程

1. npm install 安装依赖包
2. 运行模拟器
3. react-native start
4. react-native run-android / react-native run-ios

#### 使用说明

1. 安卓设备reload JS：双击R
2. IOS设备reload JS：Cmd+R

#### 项目代码结构

- App.js  根组件
- index.js 入口文件
- src
  - api  API接口文件夹
  - assets  静态资源文件夹
  - components  组件文件夹
  - redux  状态管理文件夹
    - actions   文件夹，存放各个模块的action
    - reducers  文件夹，存放各个模块的reducer
    - services  文件夹，存放各个模块的http请求接口
    - reducer.js  根reducer文件-combineReducers for rootReducer
    - store.js   项目store文件 - createStore(reducer,applyMiddleware(thunk))
  - routers
    - index.js 项目所有路由配置
    - tab.js  底栏tabbar路由配置
  - utils 工具文件夹
  - views 页面级组件文件夹
