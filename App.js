import React, {Component} from 'react';
import { Provider,connect } from 'react-redux';
import { addNavigationHelpers } from 'react-navigation';

import store from './src/redux/store';
import AppNavigator from './src/routers/index'

export default class App extends Component{
  render(){
    return(
      <Provider store={store}>
        <AppNavigator/>
      </Provider>
    )
  }
}
